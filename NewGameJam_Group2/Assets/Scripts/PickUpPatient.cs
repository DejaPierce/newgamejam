﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpPatient : MonoBehaviour 
{
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            print ("Patient picked up");
            Destroy (gameObject);
        }
    }
}

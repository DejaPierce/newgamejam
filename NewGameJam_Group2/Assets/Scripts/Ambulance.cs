﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ambulance : MonoBehaviour{

    public float moveSpeed;

    private void Start () {
        moveSpeed = 1f;

    }

    void Update () {
       transform.Translate(moveSpeed*Input.GetAxis("Horizontal")*Time.deltaTime,0f,moveSpeed*Input.GetAxis("Vertical")*Time.deltaTime);
       
    }
}
